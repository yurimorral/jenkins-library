package com.sonar.demo.build

import com.sonar.demo.utils.Constants

class ChangeVersion implements Serializable, Constants {

    def script = [:]

    ChangeVersion (script) {
        this.script = script

    }  

    def getVersion(component){
        if (component == 'backend'){
            script.script {
                env.PREVIOUS_APPLICATION_VERSION = readMavenPom(file:"${WORKSPACE}/pom.xml").getVersion()
                GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
                env.SHORT_REV = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                //BB_BRANCH = sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD').trim()
            }  
        } 
    }

    def versionUi(config){
        script.script {
            //NEXUS_REPO_URL = NEXUS_SNAPSHOT_URL
            //env.IS_RELEASE = "false"
            script.sh "npm config set git-tag-version=false"
            script.sh "npm version patch"
            def npmVersion = script.sh(returnStdout: true, script: 'node -e \"console.log(require(\'./package.json\').version);\"').trim()
            def gitCommit = script.sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
            def appVersion = "${npmVersion}-${gitCommit}-SNAPSHOT"
            script.env.appVersion = "${appVersion}"
            changeVersionUi(appVersion,config)
        }  
    }

    def changeVersionUi(appVersion,config){
        script.echo "Version: ${appVersion}"
        script.dir('src'){
            script.sh "sed -i \'s#<title>.*</title>#<title>Demo-${appVersion}</title>#\' index.html"
        }
        script.currentBuild.displayName = "${appVersion}"
        script.currentBuild.description = "${appVersion}"

    }

}