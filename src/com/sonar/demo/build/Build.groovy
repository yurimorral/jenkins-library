package com.sonar.demo.build

import com.sonar.demo.utils.Constants

class Build implements Serializable, Constants {

    private final def script = [:]

    Build (script) {
        this.script = script
    }
 
    def mvnCommand = "mvn clean install -Dmaven.test.skip=true -X"

    def buildUi(config,uicomponent){
        //script.sh 'cp -rf /var/lib/jenkins/.npmrc ./'
        script.sh 'npm install --verbose'          
        script.sh "node --max_old_space_size=${uicomponent.mem} ./node_modules/@angular/cli/bin/ng build "
    }   

    def mvnCleanInstallWithoutTest(){
        script.sh mvnCommand
    }
}