package com.sonar.demo.build

import com.sonar.demo.utils.Constants
import com.sonar.demo.utils.CustomerVariable

class TestFront implements Serializable, Constants {

    private final def script = [:]

    TestFront (script) {
        this.script = script

    }  

    def testUi9c(){
        script.echo "TEST 9C FRONT"
    }

}