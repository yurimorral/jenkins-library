package com.sonar.demo

//import com.sonar.demo.unittest.UnitTest
//import com.sonar.demo.sonar.Scan
//import com.sonar.demo.nexus.Publish
import com.sonar.demo.scm.Github
//import com.sonar.demo.utils.Notifications
import com.sonar.demo.build.Build
import com.sonar.demo.build.ChangeVersion
import com.sonar.demo.utils.CustomerVariable
import com.sonar.demo.utils.Parameters
import com.sonar.demo.tests.Tests

class Components implements Serializable {
    private final def script = [:]

    Tests tests
    //Scan sonarScan
    //Publish publish
    Github github
    //Notifications notifications
    Build build
    ChangeVersion changeVersion
    CustomerVariable customerVariable
    Parameters parameters

    Components(script) {
        this.script = script

        tests = new Tests(script)
        //sonarScan = new Scan(script)
        //publish = new Publish(script)
        github = new Github(script)
        //notifications = new Notifications(script)
        build = new Build(script)
        changeVersion = new ChangeVersion(script)
        customerVariable = new CustomerVariable(script)
        parameters = new Parameters(script)
        
    }

}