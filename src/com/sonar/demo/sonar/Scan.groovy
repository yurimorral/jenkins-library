package com.sonar.demo.sonar

class Scan implements Serializable {

    private final def script = [:]

    Scan (script) {
        this.script = script
    }

    void upload(folder) {
        script.dir(path: folder) {
            script.script {
                def scannerHome = script.tool 'sonar_scanner'
                script.sh 'echo "Running SonarQube Scanner"'
                script.withSonarQubeEnv('sonarqube') {
                    script.sh "${scannerHome}/bin/sonar-scanner -X -Dproject.settings=./sonar-project.properties"
                }
            }
        }
    }
}