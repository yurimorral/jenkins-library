package com.sonar.demo.utils

class Parameters implements Serializable {

    private final def script = [:]

    Parameters (script) {
        this.script = script
    }

    def getParameters(customer){
        script.echo "cliente echo ${customer}"
        script.params = [
        	script.string(name: "COMMIT_ID", defaultValue: '*/develop', description: 'enter commit or branch (*/branchname)'),
            script.string(name: "email", defaultValue: '', description: 'enter your email')  
        ]  
        script.properties([
            script.parameters(
                script.params
            )
        ])
    }

}