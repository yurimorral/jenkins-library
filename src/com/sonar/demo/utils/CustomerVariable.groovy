package com.sonar.demo.utils

import com.sonar.demo.utils.Constants

class CustomerVariable implements Serializable, Constants {

    private final def script =  [:]

    CustomerVariable(script) {
        this.script = script
    }

    def getCustomerProperties(String jobName) {
        def customerName = getCustomerName(jobName)
        def properties = script.libraryResource ("${customerName}.properties")
        def propertiesToMap = script.evaluate(properties)
        return propertiesToMap
    }

    private def getCustomerName(String jobName) {
        return jobName.split('-')[4]
    }

    private def getComponentName(String jobName) {
        return jobName.split('-')[5]
    }    

    def getInterface(name){
	    return getComponentName(name)
    }
}
