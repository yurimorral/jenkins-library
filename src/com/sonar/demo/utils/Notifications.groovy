package com.sonar.demo.utils

class Notifications implements Serializable {

    private final def script = [:]

    Notifications (script) {
        this.script = script
    }

    void startPipeline(jenkinsbot, jenkins_url, jenkins_name){
        script.googlechatnotification url: "id:${jenkinsbot}", message: "${jenkins_url} - ${jenkins_name} - INPROGRESS", sameThreadNotification: 'true'
    } 

    void endPipeline(config, JOB_NAME,BUILD_DISPLAY_NAME){
        script.googlechatnotification url: "id:${config.console.jenkinsbot}", message: "Hola Paulina, Un nuevo Release de ${JOB_NAME} ha sido creado ! - Repo: https://github.com/Equifax/${config.console.repository} ", sameThreadNotification: 'true'
        script.withCredentials([script.string(credentialsId: config.console.jenkinsbot, variable: 'GOOGLE_CHAT_URL')]) { //set SECRET with the credential content
             script.sendGoogleChatBuildReport(Version: BUILD_DISPLAY_NAME,
                message: "Se ha realizado una nueva compilación en nuestro fabulosisimo Jenkins." +
                "<br>Nota: Quitar la s del https al pinchar en los botones"
            )
        }
    }

    void email(config, APP_VERSION, JOB_NAME, BUILD_DISPLAY_NAME, currentBuild ,BUILD_NUMBER, BUILD_ID, GIT_BRANCH){
        script.sh 'cp -rf /var/lib/jenkins/email-templates/img/*.png ./'
		script.emailext attachLog: true, 
		mimeType: 'text/html',
		compressLog: true, 
		attachmentsPattern: '*.png',
		subject: "${currentBuild.currentResult} ${JOB_NAME} - #${BUILD_ID}", 
		to: "yuri.morral@equifax.com",
		body: """
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head>
				<!--[if (gte mso 9)|(IE)]>
				<xml>
					<o:OfficeDocumentSettings>
						<o:AllowPNG/>
						<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<title>Jenkins Notification</title>

				<!-- Google Fonts Link -->
				<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />

				<style type="text/css">

					/*------ Client-Specific Style ------ */
					@-ms-viewport{width:device-width;}
					table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
					img{-ms-interpolation-mode:bicubic; border: 0;}
					p, a, li, td, blockquote{mso-line-height-rule:exactly;}
					p, a, li, td, body, table, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
					#outlook a{padding:0;}
					.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
					.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td,img{line-height:100%;}

					/*------ Reset Style ------ */
					*{-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}
					table{border-spacing: 0 !important;}
					h1, h2, h3, h4, h5, h6, p{display:block; Margin:0; padding:0;}
					img, a img{border:0; height:auto; outline:none; text-decoration:none;}
					#bodyTable, #bodyCell{ margin:0; padding:0; width:100%;}
					body {height:100%; margin:0; padding:0; width:100%;}

					.appleLinks a {color: #c2c2c2 !important; text-decoration: none;}
					span.preheader { display: none !important; }

					/*------ Google Font Style ------ */
					[style*="Open Sans"] {font-family:'Open Sans', Helvetica, Arial, sans-serif !important;}				
					/*------ General Style ------ */
					.wrapperWebview, .wrapperBody, .wrapperFooter{width:100%; max-width:600px; Margin:0 auto;}

					/*------ Column Layout Style ------ */
					.tableCard {text-align:center; font-size:0;}
					
					/*------ Images Style ------ */
					.imgHero img{ width:600px; height:auto; }
					
				</style>

				<style type="text/css">
					/*------ Media Width 480 ------ */
					@media screen and (max-width:640px) {
						table[class="wrapperWebview"]{width:100% !important; }
						table[class="wrapperEmailBody"]{width:100% !important; }
						table[class="wrapperFooter"]{width:100% !important; }
						td[class="imgHero"] img{ width:100% !important;}
						.hideOnMobile {display:none !important; width:0; overflow:hidden;}
					}
				</style>

			</head>

			<body style="background-color:#F9F9F9;">
			<center>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;background-color:#F9F9F9;" id="bodyTable">
				<tr>
					<td align="center" valign="top" style="padding-right:10px;padding-left:10px;" id="bodyCell">
					<!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" style="width:600px;" width="600"><tr><td align="center" valign="top"><![endif]-->

					<!-- Email Wrapper Header Open //-->
					<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperWebview">
						<tr>
							<td align="center" valign="top">
								<!-- Content Table Open // -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="middle" style="padding-top:40px;padding-bottom:40px" class="emailLogo">
											<!-- Logo and Link // -->
											<a href="#" target="_blank" style="text-decoration:none;">
												<img src='cid:LogotiposEquifax.png' alt="" width="200" border="0" style="width:100%; max-width:200px;height:auto; display:block;"/>
											</a>
										</td>
									</tr>
								</table>
								<!-- Content Table Close // -->
							</td>
						</tr>
					</table>
					<!-- Email Wrapper Header Close //-->

					<!-- Email Wrapper Body Open // -->
					<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperBody">
						<tr>
							<td align="center" valign="top">

								<!-- Table Card Open // -->
								<table border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;border-color:#E5E5E5; border-style:solid; border-width:0 1px 1px 1px;" width="100%" class="tableCard">

									<tr>
										<!-- Header Top Border // -->
										<td height="3" style="background-color:#b30202;font-size:1px;line-height:3px;" class="topBorder" >&nbsp;</td>
									</tr>

									<tr>
										<td align="center" valign="top" style="padding-bottom:40px" class="imgHero">
											<!-- Hero Image // -->
											<a href="#" target="_blank" style="text-decoration:none;">
												<img src="cid:order-place.png" width="300" alt="" border="0" style="width:100%; max-width:300px; height:auto; display:block;" />
											</a>
										</td>
									</tr>

									<tr>
										<td align="center" valign="top" style="padding-bottom:5px;padding-left:20px;padding-right:20px;" class="mainTitle">
											<!-- Main Title Text // -->
											<h2 class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:28px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:36px; text-transform:none; text-align:center; padding:0; margin:0">
												Execution Completed !
											</h2>
										</td>
									</tr>

									<tr>
										<td align="center" valign="top" style="padding-bottom:30px;padding-left:20px;padding-right:20px;" class="subTitle">
											<!-- Sub Title Text // -->
											<h4 class="text" style="color:#999999; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
												${JOB_NAME} | #${BUILD_NUMBER}
											</h4>
										</td>
									</tr>
									<tr>
										<td align="center" valign="top" style="padding-bottom:20px;" class="productPrice">
											<!-- Offer Code Text// -->
											<p class="text" style="color:#003CE5; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
												Version: <strong>${APP_VERSION}</strong>
											</p>
										</td>
									</tr>						

									<tr>
										<td align="center" valign="top" style="padding-left:20px;padding-right:20px;" class="containtTable">

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableProduct">
												<tr>
													<td align="center" valign="top" style="padding-bottom:10px;" class="productName">
														<!-- Offer Title Text// -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Repository:
														</p>
													</td>
												</tr>

												<tr>
													<td align="center" valign="top" style="padding-bottom:10px;" class="productQty">
														<!-- Offer Title Text// -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															${config.console.repository}
														</p>
													</td>
												</tr>
											</table>

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableDivider">
												
												<tr>
													<td align="center" valign="top" style="padding-top:20px;padding-bottom:40px;">
														<!-- Divider // -->
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td height="1" style="background-color:#E5E5E5;font-size:1px;line-height:1px;" class="divider">&nbsp;</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableTotalTitle">
												<tr>
													<td align="center" valign="top" style="padding-bottom:5px;" class="smlTotalTitle">
														<!-- Sub Total Title Text // -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Commit/Branch : &nbsp;&nbsp;&nbsp;&nbsp;<strong>${GIT_BRANCH}</strong>
														</p>
													</td>
												</tr>

												<tr>
													<td align="center" valign="top" style="padding-bottom:5px;" class="smlTotalTitle">
														<!-- Discount Title Text // -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Changes : &nbsp;&nbsp;&nbsp;&nbsp;<strong>- ${currentBuild.changeSets.msg}</strong>
														</p>
													</td>
												</tr>

												<tr>
													<td align="center" valign="top" style="padding-bottom:5px;" class="smlTotalTitle">
														<!-- Shipping Title Text // -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Starter By : &nbsp;&nbsp;&nbsp;&nbsp;<strong>${currentBuild.changeSets.author}</strong>
														</p>
													</td>
												</tr>

												<tr>
													<td align="center" valign="top" style="padding-bottom:10px;" class="smlTotalTitle">
														<!-- State Tax Title Text // -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Execution Date : &nbsp;&nbsp;&nbsp;&nbsp;<strong>DATE</strong>
														</p>
													</td>
												</tr>

											</table>

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableDivider">
												<tr>
													<td align="center" valign="top" style="padding-top:20px;padding-bottom:40px;">
														<!-- Divider // -->
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td height="1" style="background-color:#E5E5E5;font-size:1px;line-height:1px;" class="divider">&nbsp;</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableTitleDescription">
												<tr>
													<td align="center" valign="top" style="padding-bottom:10px;" class="mediumTitle">
														<!-- Medium Title Text // -->
														<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
															Additional Information :
														</p>
													</td>
												</tr>

												<tr>
													<td align="center" valign="top" style="padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;" class="socialLinks">
														<!-- Social Links (Facebook)// -->
														<a href="#nexus-link" target="_blank" style="display:inline-block;" class="facebook">
															<img src="cid:nexus.png" alt="" width="40" border="0" style="height:auto; width:100%; max-width:40px; margin-left:2px; margin-right:2px" />
														</a>
														<!-- Social Links (Twitter)// -->
														<a href="#fortify-link" target="_blank" style="display:inline-block;" class="twitter">
															<img src="cid:fortify.png" 
															alt="" width="40" border="0" style="height:auto; width:100%; max-width:40px; margin-left:2px; margin-right:2px" />
														</a>
														<!-- Social Links (Pintrest)// -->
														<a href="#sonarqube-link" target="_blank" style="display:inline-block;" class="pintrest">
															<img src="cid:sonar.png" alt="" width="40" border="0" style="height:auto; width:100%; max-width:40px; margin-left:2px; margin-right:2px" />
														</a>
													</td>
												</tr>

											</table>

											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableButtonDate">
												<tr>
													<td align="center" valign="top" style="padding-top:20px;padding-bottom:5px;">
														<!-- Button Table // -->
														<table align="center" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td align="center" class="ctaButton" style="background-color:#003CE5;padding-top:12px;padding-bottom:12px;padding-left:35px;padding-right:35px;border-radius:50px">
																	<!-- Button Link // -->
																	<a class="text" href="#" target="_blank" style="color:#FFFFFF; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:13px; font-weight:700; font-style:normal;letter-spacing:1px; line-height:20px; text-transform:uppercase; text-decoration:none; display:block">
																		Enter to Build
																	</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>

												
											</table>

										</td>
									</tr>

									<tr>
										<td height="20" style="font-size:1px;line-height:1px;">&nbsp;</td>
									</tr>

									<tr>
										<td height="20" style="font-size:1px;line-height:1px;">&nbsp;</td>
									</tr>
								</table>
								<!-- Table Card Close// -->

								<!-- Space -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" class="space">
									<tr>
										<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
									</tr>
								</table>

							</td>
						</tr>
					</table>
					<!-- Email Wrapper Body Close // -->

					<!-- Email Wrapper Footer Open // -->
					<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperFooter">
						<tr>
							<td align="center" valign="top">
								<!-- Content Table Open// -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" class="footer">

									<tr>
										<td align="center" valign="top" style="padding-top:10px;padding-bottom:5px;padding-left:10px;padding-right:10px;" class="brandInfo">
											<!-- Brand Information // -->
											<p class="text" style="color:#777777; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:center; padding:0; margin:0;">SRE CYBERFINANCIAL
											</p>
										</td>
									</tr>

									<tr>
										<td align="center" valign="top" style="padding-top:0px;padding-bottom:10px;padding-left:10px;padding-right:10px;" class="footerEmailInfo">
											<!-- Information of NewsLetter (Subscribe Info)// -->
											<p class="text" style="color:#777777; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:center; padding:0; margin:0;" >
											If you have any quetions please contact us <a href="#" style="color:#777777;text-decoration:underline;" target="_blank">sre_cyberfinancial@equifax.com</a>
											</p>
										</td>
									</tr>

									<!-- Space -->
									<tr>
										<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
									</tr>
								</table>
								<!-- Content Table Close// -->
							</td>
						</tr>

						<!-- Space -->
						<tr>
							<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
						</tr>
					</table>
					<!-- Email Wrapper Footer Close // -->

					<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
					</td>
				</tr>
			</table>

			</center>
			</body>
			</html>
		"""
    }
	void gitlabEmail(email, repo_list, JOB_NAME, BUILD_DISPLAY_NAME, currentBuild, BUILD_ID){
        script.sh 'cp -rf /data/repository/gitlab/img/*.png ./'
		script.emailext attachLog: true, 
		mimeType: 'text/html',
		compressLog: true, 
		attachmentsPattern: '*.png',
		subject: "${currentBuild.currentResult} ${JOB_NAME} - #${BUILD_ID}", 
		to: "${email},sre_cyberfinancial@equifax.com",
		body: """
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
				<head>
					<!--[if (gte mso 9)|(IE)]>
					<xml>
						<o:OfficeDocumentSettings>
							<o:AllowPNG/>
							<o:PixelsPerInch>96</o:PixelsPerInch>
						</o:OfficeDocumentSettings>
					</xml>
					<![endif]-->
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<meta name="viewport" content="width=device-width" />
					<meta http-equiv="X-UA-Compatible" content="IE=edge" />
					<title>Notify - Notification Email Template</title>

					<!-- Google Fonts Link -->
					<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />

					<style type="text/css">

						/*------ Client-Specific Style ------ */
						@-ms-viewport{width:device-width;}
						table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
						img{-ms-interpolation-mode:bicubic; border: 0;}
						p, a, li, td, blockquote{mso-line-height-rule:exactly;}
						p, a, li, td, body, table, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
						#outlook a{padding:0;}
						.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
						.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td,img{line-height:100%;}

						/*------ Reset Style ------ */
						*{-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}
						table{border-spacing: 0 !important;}
						h1, h2, h3, h4, h5, h6, p{display:block; Margin:0; padding:0;}
						img, a img{border:0; height:auto; outline:none; text-decoration:none;}
						#bodyTable, #bodyCell{ margin:0; padding:0; width:100%;}
						body {height:100%; margin:0; padding:0; width:100%;}

						.appleLinks a {color: #c2c2c2 !important; text-decoration: none;}
						span.preheader { display: none !important; }

						/*------ Google Font Style ------ */
						[style*="Open Sans"] {font-family:'Open Sans', Helvetica, Arial, sans-serif !important;}				
						/*------ General Style ------ */
						.wrapperWebview, .wrapperBody, .wrapperFooter{width:100%; max-width:600px; Margin:0 auto;}

						/*------ Column Layout Style ------ */
						.tableCard {text-align:center; font-size:0;}
						
						/*------ Images Style ------ */
						.imgHero img{ width:600px; height:auto; }
						
					</style>

					<style type="text/css">
						/*------ Media Width 480 ------ */
						@media screen and (max-width:640px) {
							table[class="wrapperWebview"]{width:100% !important; }
							table[class="wrapperEmailBody"]{width:100% !important; }
							table[class="wrapperFooter"]{width:100% !important; }
							td[class="imgHero"] img{ width:100% !important;}
							.hideOnMobile {display:none !important; width:0; overflow:hidden;}
						}
					</style>

				</head>

				<body style="background-color:#F9F9F9;">
				<center>

				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;background-color:#F9F9F9;" id="bodyTable">
					<tr>
						<td align="center" valign="top" style="padding-right:10px;padding-left:10px;" id="bodyCell">
						<!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" style="width:600px;" width="600"><tr><td align="center" valign="top"><![endif]-->

						<!-- Email Wrapper Header Open //-->
						<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperWebview">
							<tr>
								<td align="center" valign="top">
									<!-- Content Table Open // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="middle" style="padding-top:40px;padding-bottom:40px" class="emailLogo">
												<!-- Logo and Link // -->
												<a href="#" target="_blank" style="text-decoration:none;">
													<img src="cid:LogotiposEquifax.png" alt="" width="150" border="0" style="width:100%; max-width:150px;height:auto; display:block;"/>
												</a>
											</td>
										</tr>
									</table>
									<!-- Content Table Close // -->
								</td>
							</tr>
						</table>
						<!-- Email Wrapper Header Close //-->

						<!-- Email Wrapper Body Open // -->
						<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperBody">
							<tr>
								<td align="center" valign="top">

									<!-- Table Card Open // -->
									<table border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;border-color:#E5E5E5; border-style:solid; border-width:0 1px 1px 1px;" width="100%" class="tableCard">

										<tr>
											<!-- Header Top Border // -->
											<td height="3" style="background-color:#b30202;font-size:1px;line-height:3px;" class="topBorder" >&nbsp;</td>
										</tr>

										<tr>
											<td align="center" valign="top" style="padding-bottom:40px" class="imgHero">
												<!-- Hero Image // -->
												<a href="#" target="_blank" style="text-decoration:none;">
													<img src="cid:notification-download.png" width="300" alt="" border="0" style="width:100%; max-width:300px; height:auto; display:block;" />
												</a>
											</td>
										</tr>

										<tr>
											<td align="center" valign="top" style="padding-bottom:5px;padding-left:20px;padding-right:20px;" class="mainTitle">
												<!-- Main Title Text // -->
												<h2 class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:28px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:36px; text-transform:none; text-align:center; padding:0; margin:0">
													${email}
												</h2>
											</td>
										</tr>

										<tr>
											<td align="center" valign="top" style="padding-bottom:30px;padding-left:20px;padding-right:20px;" class="subTitle">
												<!-- Sub Title Text // -->
												<h4 class="text" style="color:#999999; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:26px; text-transform:none; text-align:center; padding:0; margin:0">
													Your Code is already zipped and you can download now.
												</h4>
											</td>
										</tr>

										<tr>
											<td align="center" valign="top" style="padding-left:20px;padding-right:20px;" class="containtTable">

												<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableBigTitle">
													<tr>
														<td align="center" valign="top" style="padding-bottom:10px;" class="bigTitle">
															<!-- Title Text // -->
															<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:28px; font-weight:600; font-style:normal; letter-spacing:normal; line-height:44px; text-transform:none; text-align:center; padding:0; margin:0">
																Name: ${repo_list}
															</p>
														</td>
													</tr>

												</table>

												<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableButtonDate">
													<tr>
														<td align="center" valign="top" style="padding-top:20px;padding-bottom:5px;">
															<!-- Button Table // -->
															<table align="center" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td align="center" class="ctaButton" style="background-color:#003CE5;padding-top:12px;padding-bottom:12px;padding-left:35px;padding-right:35px;border-radius:50px">
																		<!-- Button Link // -->
																		<a class="text" href="https://172.24.98.106/repository/gitlab/code" target="_blank" style="color:#FFFFFF; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:13px; font-weight:700; font-style:normal;letter-spacing:1px; line-height:20px; text-transform:uppercase; text-decoration:none; display:block">
																			Download Now
																		</a>
																	</td>
																</tr>
															</table>
														</td>
													</tr>

													<tr>
														<td align="center" valign="top" style="padding-bottom:20px;" class="infoDate">
															<!-- Info Date // -->
															<p class="text" style="color:#000000; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:11px; font-weight:700; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:center; padding:0; margin:0">
																Deleted on : Monday Wednesday and Friday 
															</p>
														</td>
													</tr>
												</table>

											</td>
										</tr>

										<tr>
											<td height="20" style="font-size:1px;line-height:1px;">&nbsp;</td>
										</tr>
									</table>
									<!-- Table Card Close// -->

									<!-- Space -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="space">
										<tr>
											<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>
						<!-- Email Wrapper Body Close // -->

								<!-- Email Wrapper Footer Open // -->
								<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;" width="100%" class="wrapperFooter">
									<tr>
										<td align="center" valign="top">
											<!-- Content Table Open// -->
											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="footer">
						
												<tr>
													<td align="center" valign="top" style="padding-top:10px;padding-bottom:5px;padding-left:10px;padding-right:10px;" class="brandInfo">
														<!-- Brand Information // -->
														<p class="text" style="color:#777777; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:center; padding:0; margin:0;">SRE CYBERFINANCIAL
														</p>
													</td>
												</tr>
						
												<tr>
													<td align="center" valign="top" style="padding-top:0px;padding-bottom:10px;padding-left:10px;padding-right:10px;" class="footerEmailInfo">
														<!-- Information of NewsLetter (Subscribe Info)// -->
														<p class="text" style="color:#777777; font-family:'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; font-weight:400; font-style:normal; letter-spacing:normal; line-height:20px; text-transform:none; text-align:center; padding:0; margin:0;" >
														If you have any quetions please contact us <a href="#" style="color:#777777;text-decoration:underline;" target="_blank">sre_cyberfinancial@equifax.com</a>
														</p>
													</td>
												</tr>
						
												<!-- Space -->
												<tr>
													<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
												</tr>
											</table>
											<!-- Content Table Close// -->
										</td>
									</tr>
						
									<!-- Space -->
									<tr>
										<td height="30" style="font-size:1px;line-height:1px;">&nbsp;</td>
									</tr>
								</table>
								<!-- Email Wrapper Footer Close // -->
						<!-- Email Wrapper Footer Close // -->

						<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
						</td>
					</tr>
				</table>

				</center>
				</body>
				</html>
		"""
	}
}