package com.sonar.demo.scm

import com.sonar.demo.utils.CustomerVariable
import com.sonar.demo.utils.Constants
import jenkins.model.*

class Github implements Serializable, Constants {

    private final def script = [:]

    Github (script) {
        this.script = script
    } 

    private def checkOutCode(String commit, Map chekoutParams) {
            return script.checkout([
            $class                           : 'GitSCM',
            branches                         : [[name: "${commit}"]],
            doGenerateSubmoduleConfigurations: false,
            extensions                       : [[$class: 'CloneOption', timeout: 240]],
            gitTool                          : 'Default',
            submoduleCfg                     : [],
            userRemoteConfigs                : [[credentialsId: Constants.gitAccount, url: "${chekoutParams.srcrepo}"]]
        ])
    }


}