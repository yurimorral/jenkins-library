#!/usr/bin/groovy
import com.sonar.demo.*
import com.sonar.demo.utils.Constants

def call() {
    private final def config
    private final def notifications
    def jenkins = new Components(this)
    private final String component = "ui"
    
    pipeline {
	    agent { node { label 'master' } }
        tools {
            maven 'apache-maven-default'
            nodejs 'node-v12.20.0'
        }        
        options {
            timestamps()
            disableConcurrentBuilds()  //each branch has 1 job running at a time
            buildDiscarder(logRotator(numToKeepStr: '7'))
        }
        stages{
            stage('Get Customer Properties') {                         
                steps {
                    script {
                        config = new utils.CustomerVariable(this)
                            .getCustomerProperties(/* env.JOB_NAME */"cicd-pipeline-cl-9c-hites-console-ui-sre")
                        echo "Propiedades Cliente: ${config}"
                    }
                }
            }
            stage('Get Customer Parameters') {                         
                steps {
                    script {
						jenkins.parameters.getParameters9c(config.customer)
                        echo "params: ${email} appVersion: $APP_VERSION"
                    }
                }
            }
			stage ('Confirm Build') {
                when {
                    expression {
                        type_of_build ==~ /Release/ || type_of_build ==~ /PreRelease/
                    }
                }
                steps {
                    timeout(time: 5, unit: 'MINUTES') {
                        input 'BUILD A RELEASE FROM DEVELOP BRANCH?'
                    }
                }              
            }            
            stage('Clone Code') {                  
                steps {
                    script {               
                        //jenkins.notifications.startPipeline(config, BUILD_URL, BUILD_DISPLAY_NAME)				5JVfRT8YJeNG7yV8j2vw		                         
                        jenkins.github.cleanWorkspace()
                        jenkins.github.checkOut(COMMIT_ID, config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)], type_of_build) 
                    }
                }
            } 
						
			stage('Determine and Change version') {									
				steps {
					script{
						jenkins.changeVersion.getVersion9c(component)
						jenkins.changeVersion.versionUi9c(component,type_of_build,config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)])
					}
				}			
			}           	            
/*            stage('Build') {
				steps{
					script {
						echo "build"
						jenkins.build.buildUi9c(config,type_of_build,config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)])
					}
				}
            } */
            stage('TEST FRONT') {
                steps{
					script{
						jenkins.testFront.testUi9c()
					}
                }
            }
/* 			stage('Sonar Static Analisys') {
				steps {
				sh "export no_proxy=nonprod.intl-cam.aws.efx,equifax.com"
				echo "Sonar Static Analisys" //sh "mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar -Psonar -Dsonar.projectKey=${env.APP_SQ_KEY}:${params.COMMIT_ID}"
					script {
						jenkins.sonarScan.upload("")
					}
				}
			}		
            stage('Fortify Analisys FrontEnd') {
                when {
                    expression {
                        fortify_scan ==~ /Yes/ ||  type_of_build ==~ /Release/

                    }
                }            
                environment {
                    appSection = "ui"
                }
                steps {
                    script {
                        jenkins.analysis.upload("${appSection}", "${component}", config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)],config.customer)
                    }
                }
            }             
            stage('Fortify Analisys Results FrontEnd') {
                when {
                    expression {
                        fortify_result ==~ /Yes/ ||  type_of_build ==~ /Release/
                    }
                }
                steps {
                    script {
                        jenkins.analysisResults.downloadFortify(config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)],config.customer)
                    }
                }                                                                                                                                                    
            }
			
 			stage('SonarQube QualityGate'){
				steps {
					echo "Sonar Quality Gate"
					script{
						jenkins.sonarScan.qualityGate("")
					}
				}
			}
      						
			stage('Docker build image'){					
				steps {
                    script{
                        jenkins.docker.build(config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)],config.customer,component, WORKSPACE,type_of_build,appVersion)
                    }
				}	
			}
            
			stage('Publish Nexus') {			
				agent { 
                	label 'kubectl'
            	}
				steps {
                    script {
                        jenkins.publish.nexusSnapshot9c(config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)],type_of_build,component,appVersion)
                    }                    
				}
			}			
			stage('Deployment') {
				agent { 
                	label 'kubectl'
            	}				  
				steps {
                    script{
                        jenkins.deploy.deploy9C(config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)],type_of_build,component,"dev",appVersion)
                    }
				}
			}        */    
            stage('Perform Git Tag and Push to Remote') {
                steps {
                    script{
                        jenkins.github.tag(config[jenkins.customerVariable.getInterface("cicd-pipeline-cl-9c-hites-console-ui-sre",component)], APP_VERSION, COMMIT_ID, type_of_build)
                    }
					echo "GIT"
                    //cloudPublishGithubRelease()
                }
            }
        }
        post {
            always {
                script {
                    currentBuild.result = currentBuild.result ?: 'SUCCESS'
                    //jenkins.notifications.endPipeline(config, JOB_NAME,BUILD_DISPLAY_NAME)
                    //jenkins.notifications.email(config, APP_VERSION, JOB_NAME, BUILD_DISPLAY_NAME, currentBuild ,BUILD_NUMBER, BUILD_ID, GIT_BRANCH)
                }
            }
	    }
    } 

}
