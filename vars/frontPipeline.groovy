#!/usr/bin/groovy
import com.sonar.demo.*
import com.sonar.demo.utils.Constants

def call() {
    private final def config
    private final def notifications
    def jenkins = new Components(this)
    
    pipeline {
	    agent { node { label 'master' } }
        tools {
            maven 'maven'
            nodejs 'node'
        }        
        options {
            timestamps()
            disableConcurrentBuilds()  //each branch has 1 job running at a time
            buildDiscarder(logRotator(numToKeepStr: '7'))
        }
        stages{
            stage('Get Customer Properties') {                         
                steps {
                    script {
                        config = new utils.CustomerVariable(this).getCustomerProperties(env.JOB_NAME)
                        echo "Propiedades Cliente: ${config}"
                    }                    
                }
            }
            stage('Get Customer Parameters') {                         
                steps {
                    script {
						jenkins.parameters.getParameters(config.customer)
                    }
                }
            }          
            stage('Clone Code') {                  
                steps {
                    script {               
                        //jenkins.notifications.startPipeline(config, BUILD_URL, BUILD_DISPLAY_NAME)						                         
                        cleanWs()
                        jenkins.github.checkOutCode(COMMIT_ID, config[jenkins.customerVariable.getInterface(env.JOB_NAME)]) 
                    }
                }
            } 
						
			stage('Determine and Change version') {									
				steps {
					script{
						jenkins.changeVersion.getVersion(config[jenkins.customerVariable.getInterface(env.JOB_NAME)])
						jenkins.changeVersion.versionUi(config[jenkins.customerVariable.getInterface(env.JOB_NAME)])
					}
				}			
			}           	            
            stage('Build') {
				steps{
					script {
						echo "build"
						jenkins.build.buildUi(config,config[jenkins.customerVariable.getInterface(env.JOB_NAME)])
					}
				}
            } 
            stage('TEST FRONT') {
                steps{
					script{
						jenkins.test.testUi()
					}
                }
            }
/* 			stage('Sonar Static Analisys') {
				steps {
				sh "export no_proxy=nonprod.intl-cam.aws.efx,equifax.com"
				echo "Sonar Static Analisys" //sh "mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.7.0.1746:sonar -Psonar -Dsonar.projectKey=${env.APP_SQ_KEY}:${params.COMMIT_ID}"
					script {
						jenkins.sonarScan.upload("")
					}
				}
			}		
 			stage('SonarQube QualityGate'){
				steps {
					echo "Sonar Quality Gate"
					script{
						jenkins.sonarScan.qualityGate("")
					}
				}
			}				
			stage('Docker build image'){					
				steps {
                    script{
                        jenkins.docker.build(config[jenkins.customerVariable.getInterface(env.JOB_NAME,component)],config.customer,component, WORKSPACE,type_of_build,appVersion)
                    }
				}	
			}			
			stage('Deployment') {
				agent { 
                	label 'kubectl'
            	}				  
				steps {
                    script{
                        jenkins.deploy.deploy9C(config[jenkins.customerVariable.getInterface(env.JOB_NAME,component)],type_of_build,component,"dev",appVersion)
                    }
				}
			}            
            stage('Perform Git Tag and Push to Remote') {
                steps {
                    script{
                        jenkins.github.tag(config[jenkins.customerVariable.getInterface(env.JOB_NAME,component)], APP_VERSION, COMMIT_ID, type_of_build)
                    }
					echo "GIT"
                    //cloudPublishGithubRelease()
                }
            }*/
        }
        post {
            always {
                script {
                    currentBuild.result = currentBuild.result ?: 'SUCCESS'
                    //jenkins.notifications.endPipeline(config, JOB_NAME,BUILD_DISPLAY_NAME)
                    //jenkins.notifications.email(config, APP_VERSION, JOB_NAME, BUILD_DISPLAY_NAME, currentBuild ,BUILD_NUMBER, BUILD_ID, GIT_BRANCH)
                }
            }
	    }
    } 

}
